# -*- coding: utf-8 -*-
"""
Main script for computing recommendations

@author: icaromarley5
"""

'''
Examples 
python main.py -t 'Blinded in chains' -a 'Avenged Sevenfold' 

python main.py -t 'Toxicity' -a 'System of a Down' 

python main.py -t 'Dirty Magic' -a 'The Offspring'

python main.py -t 'My Soul to Keep' -a 'The Creepshow'

python main.py -t 'Pupila' -a 'Anavitória'

python main.py -t 'Bixinho' -a 'DUDA BEAT'

python main.py -t 'Before I forget' -a 'Slipknot'

python main.py -t 'Believer' -a 'Imagine Dragons'

python main.py -t 'Enter Sandman' -a 'Metallica'  

python main.py -t 'Meteoro' -a 'Luan Santana'  

python main.py -t '' -a '' 
'''

import time
from recommendations import get_recommendations,print_recommendations
import sys

def main(artist,title,limit):
    start = time.time()
    profile, recommendations, status = get_recommendations(title,artist,limit)
    end = time.time()    
    print('Time passed: {} seconds'.format(end - start))
    if not status['error']:
        print_recommendations(profile,recommendations)
    else:
        print('Processing error:',status['error'])
        
if __name__ =='__main__': 
    argv = sys.argv
    try:
        if argv[1] == '-t':
            title = argv[2]
        if argv[3] == '-a':
            artist = argv[4]
        if len(sys.argv)==5:
            limit = 10
        elif argv[5] == '-l':
            limit = int(argv[6])
        title, artist
    except:
        print('main.py -t <track_title> -a <artist_name> -lastindex() <limit>')
        sys.exit(1)
    print('Getting {} recommendations for {} by {}\n'.format(limit,title,artist))
    main(artist,title,limit)